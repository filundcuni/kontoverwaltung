19.12.2015 | erstellung der Readme, Contributing und changelog datei

22.12.2015 | Besprechung des Aufbaus und der Funktionalitäten des Programms

26.12.2015 | Problemlösung von Speichern des Kontostandes und Zugriff
auf diesen über eine Funktion

31.12.2015 | Erstellung der restlichen grundlegenden Programmfunktionalitäten

06.01.2015 | Fertigstellung des grundlegenden Programms (Menu fuer Zugriff auf 
die Funktionalitäten)

09.01.2015 | Fertigstellung der restlichen Programmlogik und Upload in GitLab
