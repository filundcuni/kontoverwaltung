Team:   Bürger, Philipp
        Stasek, Christopher
        
Verteilung:

Bürger: Das Problem der langfristigen Speicherung lösen um den bestmöglichen
        Weg zu finden einen guten übergang zwischen den einzelnen 
        Kontomethoden(Verwaltungsmethoden) und dem/den Dokumenten zur 
        Speicherung zu finden.
        
Stasek: Die Verwaltungsmethoden( z.B. Einzahlen o. Auszahlen) von der
        Grundlogik aufbauen und damit diese Fehlerfrei funktionieren und man
        dadurch mögliche Fehler auf die Übertragung zwischen den 
        Methoden/Dokumenten eingrenzen kann.