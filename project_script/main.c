
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "funktionen.c"



void menu()
{
    int auswahl;
    double betrag;
    int z;
    int p, pin_alt, pin_neu, pin_neu_2;
    do
    {
        //Menüstruktur
        system("clear");
        printf("Kontostand anzeigen - 1\n");
        printf("Einzahlen - 2\n");
        printf("Auszahlen - 3\n");
        printf("Pin aendern - 4\n");
        printf("\n");
        printf("Beenden - 0\n");
        
        //Eingabe der gewünschten Zahl (Menüpunkt)
        printf("Bitte Zahl eingeben: \n");
        scanf("%d", &auswahl);
        
        //Auswahl der jeweiligen Funktionalität
        switch (auswahl)
        {
            case 1:
                //Kontostand anzeigen
                system("clear");
                printf("Der aktuelle Kontostand betraegt: %.2f\n", getKontostand());
                sleep(5);
                break;
            case 2:
                //Einzahlen
                system("clear");
                printf("Bitte Betrag eingeben: ");
                scanf("%lf", &betrag);
                kontostandAendern('+', betrag);
                printf("Einzahlung erfolgreich\n");
                printf("\nDer akutelle Kontostand betraegt: %.2f\n", getKontostand());
                sleep(4);
                break;
            case 3:
                //Auszahlen
                system("clear");
                printf("Bitte Betrag eingeben: ");
                scanf("%lf", &betrag);
                z = kontostandAendern('-', betrag);
                if (z == 0)
                {
                    printf("Auszahlung erfolgreich\n");
                    printf("\nDer akutelle Kontostand betraegt: %.2f\n", getKontostand());
                }else
                    printf("Fehler beim auszahlen\n");
                sleep(4);
                break;
            case 4:
                //Pin ändern
                system("clear");
                printf("Bitte alten Pin eingeben: \n");
                scanf("%d", &pin_alt);
                printf("Bitte neuen Pin eingeben: \n");
                scanf("%d", &pin_neu);
                printf("Bitte neuen Pin wiederholen: \n");
                scanf("%d", &pin_neu_2);
                p = pinAendern(pin_alt, pin_neu, pin_neu_2);
                if (p == 1)
                    printf("Pin erfolgreich geaendert!\n");
                else
                    printf("Falsche Eingabe!\n");
                sleep(4);
                break;
            case 0:
                //Programm Beenden
                system("clear");
                exit(0);
        }
        
    }while (auswahl != 0);
}


int main(int argc, const char * argv[])
{
    
    int pin;
    do
    {
        //Überprüfung des Pins und öffen des Menüs
        system("clear");
        printf("Pin eingeben: \n");
        scanf("%d", &pin);
        if (pin == getPin())
            menu();
        else
        {
            printf("Falscher Pin\n");
            sleep(3);
        }
    }while (pin != getPin());
    
   
    
}




