#include <stdio.h>

void setKontostand(double z)
{
    //Verbindung zur Datei kontostand.txt
    FILE *fp = fopen("/Users/philippburger/Documents/Studium/Erstes_Semester/Programmieren/Projekt_Konto/kontostand.txt", "w");
    
    //Überprüfung der Verbindung und Schreiben der Datei
    if (fp != NULL)
    {
        fprintf(fp, "%.2f", z);
        fclose (fp);
    }
    
}

void setPin(int p)
{
    //Verbindung zur Datei pin.txt
    FILE *fp = fopen("/Users/philippburger/Documents/Studium/Erstes_Semester/Programmieren/Projekt_Konto/pin.txt", "w");
    
    //Überprüfung der Verbindung und Schreiben der Datei
    if (fp != NULL)
    {
        fprintf(fp, "%d", p);
        fclose(fp);
    }
}

double getKontostand()
{
    double kontostand;
    
    //Verbindung zur Datei kontostand.txt
    FILE *fp = fopen("/Users/philippburger/Documents/Studium/Erstes_Semester/Programmieren/Projekt_Konto/kontostand.txt", "r");
    
    //Überprüfung der Verbindung und auslesen der Datei
    if (fp != NULL)
    {
        fscanf(fp, "%lf", &kontostand);
        fclose(fp);
    }
    
    return kontostand;
}

int getPin()
{
    int pin;
    
    //Verbindung zur Datei pin.txt
    FILE *fp = fopen("/Users/philippburger/Documents/Studium/Erstes_Semester/Programmieren/Projekt_Konto/pin.txt", "r");
    
    //Überprüfung der Verbindung und auslesen der Datei
    if (fp != NULL)
    {
        fscanf(fp, "%d", &pin);
        fclose(fp);
    }
    
    return pin;
}

int kontostandAendern(char c, double b)
{
    double ks_neu;
    
    //Überprüfung welche Operation durchgeführt werden soll
    if (c == '+')
    {
        //Einzahlung
        ks_neu = getKontostand() + b;
        setKontostand(ks_neu);
    }else if (c == '-')
    {
        //Überprüfung und Auszahlung
        if (getKontostand() - b >= 0)
        {
            ks_neu = getKontostand() - b;
            setKontostand(ks_neu);
            return 0;
        }
        
        return 1;
    }
    
    return 2;
}

int pinAendern(int p_Alt, int p_Neu, int p_Neu_2)
{
    //Überprüfung und Änderung des Pins
    if (p_Alt == getPin() && p_Neu == p_Neu_2)
    {
        setPin(p_Neu);
        return 1;
    }else
        return 0;
    
}