Projektname: f & c Kontoverwaltung

Das Programm simuliert eine Kontoverwaltung bei welcher eine Person Geld einzahlen, auszahlen 
und sich den Kontostand anzeigen lassen kann. Dabei wird viel wert auf Details gelegt wie zum Beispiel
dem Anzeigen des aktuellen Kontostandes nach dem Ein- bzw. Auszahlen oder einer kleinen Wartezeit bevor
der Benutzer wieder in das Hauptmenü weitergeleitet wird.

Die größte Schwierigkeit wird es sein die Daten langfristig zu speichern und
problemlos wieder aufrufen zu können.